import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './containers/App';
import VtoonEditor from './components/VtoonEditor/VtoonEditor';

import configureStore from './store/configureStore';

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <App>
      <VtoonEditor />
    </App>
  </Provider>
  ,document.getElementById('root')
);

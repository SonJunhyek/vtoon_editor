import React from 'react';


class App extends React.Component { 
  render() {
    return (
      <section
        style={{
          height:'100%'
      }}>

        <div id='body'
          style={{
            flex:'1 1 auto',
            overflow: 'auto',
        }}>
          {this.props.children}
        </div>
      
      </section>
    );
  }
};

App.propTypes = {
  children: React.PropTypes.node,
};

export default App;
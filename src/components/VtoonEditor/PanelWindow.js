import React from 'react';
import {connect} from 'react-redux';
import Pipe from './Pipe';

class PanelWindow extends React.Component {
  constructor(props){
    super(props);
  }

  onChangePanelValue(e){
    var copiedPanel = Object.assign({},this.props.selectedPanel);
    copiedPanel.imageCSS[e.target.name]=e.target.value;
    this.props._updatePanelValue(copiedPanel);
  }

  render(){
    var selectedPanel = this.props.selectedPanel;
    console.log(selectedPanel);
    if (selectedPanel){
      var imageCSS = selectedPanel.imageCSS;

      return(
        <div
          style={{
            flex:"1 1 auto",
            display:"flex",
            flexDirection:"column"}}
        >
          <Pipe />
        </div>
      )
    } else return <div></div>
  }
}

// action
function updateSelectedPanel( panel){
  return({
    type: "UPDATE_SELECTED_PANEL",
    panel,
  })
}

// redux
function mapStateToProps(state){
  return({
    selectedPanel: state.vtoonData.panels[state.vtoonData.selectedPanelId],
    selectedPanelId: state.vtoonData.selectedPanelId,
  })
}

function mapDispatchToProps(dispatch){
  return({
    _updatePanelValue:(panel)=>{
      dispatch(updateSelectedPanel(panel))
    }
  })
}

PanelWindow = connect(mapStateToProps,mapDispatchToProps)(PanelWindow);
export default PanelWindow;

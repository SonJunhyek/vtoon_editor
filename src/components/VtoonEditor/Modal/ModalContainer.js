import React from 'react';
import { connect } from 'react-redux';
import {Popover,Modal,Tooltip,Button,OverlayTrigger,Collapse,Well} from 'react-bootstrap';

class ModalContainer extends React.Component {
  constructor(props){
    super(props);
  }

  load(){
    this.props._closeModal();        
  }

  close(){
    this.props._closeModal();
  }

  render(){
    console.log(this.props.modalContents);
    return(
      <div>
        <Modal show={this.props.showModal} style={{marginTop:'64px'}} onHide={this.props._closeModal.bind(this)}>
          <Modal.Header closeButton >
            <Modal.Title>Load Video</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>
              {this.props.contents}
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.load.bind(this)}>Load</Button>
            <Button onClick={this.close.bind(this)}>Close</Button>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}

// action
function closeModal(){
  return({
    type: "CLOSE_MODAL",
  })
}

// redux
function mapStateToProps(state){
  return({
    showModal: state.vtoonData.showModal,
    modalContents: state.vtoonData.modalContents,
  });
}

function mapDispatchToProps(dispatch){
    return({
        _closeModal: ()=>{dispatch(closeModal())}
    });
}

ModalContainer = connect(mapStateToProps, mapDispatchToProps)(ModalContainer);
export default ModalContainer;
import React from 'react';
import {connect} from 'react-redux';

class VideoBoard extends React.Component {
  constructor(props){
    super(props);
  }

  componentDidUpdate(){
    // store에서 video 정보 변경시 비디오 src정보 업데이트
    document.getElementById("video").load();
  }

  render(){
    return (
      <div className="embed-responsive embed-responsive-16by9">
        <video id="video" className="embed-responsive-item" controls>
          <source src={this.props.videoInfo.url || ""} alt="" type="video/mp4" />
          Your browser does not support HTML5 video.
        </video>
      </div>
    )
  }
}

// redux
function mapStateToProps(state) {
  return {
    videoName: state.vtoonData.videoName,
    videoInfo: state.vtoonData.videoInfo,
  }
}

VideoBoard = connect(mapStateToProps)(VideoBoard);
export default VideoBoard;

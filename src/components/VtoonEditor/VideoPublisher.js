import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

// constants
import {SERVER_URL} from '../../constants'
class VideoPublisher extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      title: "JHtest",
      episode: "5ndTest",
      style: "VtoonTranlsformStyle2",
    }

  }

  publish(){
    console.log("publish()");
    var that = this;
    var WORKER_API = SERVER_URL+"/api/worker";
    var fppInterval = 10;
    var fppCum =0;
    //publish 할 json data 생성

    var store = this.props.store;
    var publish = {
      title: this.props.store.videoName,
      transition: [],
      fpp: [],
      images: [ // 중첩 될 이미지의 어레이
      ],
      resolution: [
        this.props.store.pageWidth,
        0
      ],
      descriptions: ""
    };
    //publish data 채우기
      // 방식 1. panel의 이미지를 그대로 이용하는법
      // 업로드 할 필요 없음, images에 panel의 최상위 image url정보만 넘기면 됨
    store.pages.map( // 모든 page의 모든 panel의 url과 fpp를 publish 에 세팅함
      (page)=>{
        page.panels.map(
          (panelId)=>{
            let imageUrls = store.panels[panelId].imageUrls;
            let imageUrl = imageUrls[imageUrls.length-1]
            let fileNameArray = imageUrl.split("/");
            let fileName = fileNameArray.pop();
            fileName = fileNameArray.pop() + "." + fileName

            publish.images.push([fileName]);
            publish.fpp.push((fppCum++)*fppInterval);
          }
        )
      }
    )

      // 방식 2. page를 캡쳐하여 보내는 방법
      // 모든 page div에 대해 업로드 필요
      // 업로드 된 경로를 publish의 images 에 주어야 함

    // store에 publish 정보 저장
    that.props._postPublishData(publish);

    console.log("publish updated store check", store);

    // axios로 서버의 worker 정보 업데이트
    axios.put(WORKER_API + "/" + store.workerId, store)
    .then((res) => {
      console.log(res);
    })
    .catch((error) => {
      console.log(error)
    });

    // publish 요청
    let PUBLISH_API = WORKER_API+"/"+store.workerId+"/publish?";
    PUBLISH_API += that.state.title ? "title=" + that.state.title + "&" : "";
    PUBLISH_API += that.state.episode ? "episode=" + that.state.episode + "&" : "";
    PUBLISH_API += that.state.style ? "style=" + that.state.style : "";

    axios.put(PUBLISH_API)
    .then((res) => {
      console.log("publish request succeed with ", res);
    })
    .catch((error) => {
      console.log("publish request was failed with ", error);
    })

    // worker 정보 업데이트 요청.then -> publish axios 요청
  }

  render(){
    return(
      <div
      style={{
        width:"100%"
      }}>
        <button
          className="btn"
          style= {{width: "100%"}}
          onClick={this.publish.bind(this)}
        >Publish</button>

      </div>
    )
  }
}

// actions
function postPublishData(publishData){
  return({
      type: "POST_PUBLISH_DATA",
      publishData: publishData,
  })
}

// redux
function mapStateToProps(state){
    return({
      store: state.vtoonData,
    })
}

var mapDispatchToProps = (dispatch) => {
  return {
    _postPublishData: (publishData) => {
      dispatch(postPublishData(publishData));
    }
  }
}

VideoPublisher = connect(mapStateToProps, mapDispatchToProps)(VideoPublisher);
export default VideoPublisher;

import React from 'react';
import axios from 'axios';
import {connect} from 'react-redux';
import {SERVER_URL} from '../../constants';
import {Popover,Modal,Tooltip,Button,OverlayTrigger,Collapse,Well} from 'react-bootstrap';

import $ from 'jquery';

class PageDecorator extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      recommendedElements: [],
      addPanelshow: false,
    };
  }

  onClickRemoveButton(e){
    var index = this.props.index;
    this.props._removePage(index);
  }

  onClickAddPanel(e){
    let that =this;
    let currentPageIndex = that.props.index;
    let currentPage = that.props.page;
    let pages = that.props.pages;
    let recommendedElements =[];


    //이전 페이지의 마지막, 이후 페이지의 처음 판넬 시간정보, frame numbering 정보를 얻어 그 사이에 있는 이미지를 axios 요청
    let startTime, endTime = 0;
        startTime = currentPageIndex == 0 ? currentPage.startTime : pages[currentPageIndex-1].endTime;
        endTime = currentPageIndex == pages.length-1 ? currentPage.endTime+5 : pages[currentPageIndex+1].startTime;

    let startFrame, endFrame = 0;
        startFrame = currentPageIndex == 0 ? currentPage.startFrame : pages[currentPageIndex-1].endFrame;
        endFrame = currentPageIndex == pages.length-1 ? currentPage.endFrame+5 : pages[currentPageIndex+1].startFrame;

//    //AddPanelWindow 를 열면서 응답(recommended images)data 리스트를 넘김
//     let API = SERVER_URL+"/api/recommend/"+ that.props.videoName +
//     "?percentOfFrames="+0.1+
//     "&skipFrames="+0+
//     "&fromFrame="+startFrame+
//     "&toFrame="+endFrame;
    let workerId = that.props.workerId;

    let WORKER_API = SERVER_URL+"/api/worker";
    let RECOMMEND_API = WORKER_API+"/"+workerId+"/recommend?";
    RECOMMEND_API = RECOMMEND_API + "&percentOfFrames=" + 1;
    RECOMMEND_API = RECOMMEND_API + "&skipFrames="     + 0;
    RECOMMEND_API = RECOMMEND_API + "&fromFrame="      + startFrame;
    RECOMMEND_API = RECOMMEND_API + "&toFrame="        + endFrame;
    RECOMMEND_API = RECOMMEND_API + "&metaType="       + 0;
    if(null) RECOMMEND_API = RECOMMEND_API + "&preset="+ that.state.preset;
    RECOMMEND_API = RECOMMEND_API + "&detail="         + 0;
    console.log(RECOMMEND_API);


    // (데이터) store.vtoonData.rawImages에 클릭 한 장면 전,후 추천된 장면 사이의 데이터를 불러온다
    axios.get(RECOMMEND_API).then((res)=>{
      // worker로 image 이동 [PUT] SERVER_URL+/api/worker/{taskid}/recommend?store=1
      axios.put(SERVER_URL+"/api/worker/"+workerId+"/recommend?store=1")
      .then((res)=>{
          console.log(res);

          let recommendedImages = res.data;
          let aspectRatio = that.props.videoInfo.width/that.props.videoInfo.height;
          recommendedImages.map((recommendedImage,index)=>{

          console.log(recommendedImage.url[1]);

          recommendedElements.push(
              <img
              style={{
                  width:"100%",
                  margin:"0 auto",
              }}
              src={recommendedImage.url[1]}
              onError={
                  (e)=>{
                      e.currentTarget.src=recommendedImage.url[1];
                  }
              }
              onClick={(e)=>{

//판넬을 대체하는 버젼젼
                  let replacePanelId = that.props.page.panels[0];
                  console.log(that.props.panels[replacePanelId]);
                  let updatedPanel = Object.assign({},that.props.panels[replacePanelId]);
                  updatedPanel.imageUrls = [...updatedPanel.imageUrls,recommendedImage.url[1]];
                  console.log(replacePanelId, updatePanel);
                  that.props._updatePanel(replacePanelId, updatedPanel);


// 판넬을 새롭게 추가하는 버젼
                  // //새로운 판넬이 아이디 할당, 자료 생성하여 판넬스에 저장
                  // var newPanelId = "panel-"+ that.props.panelIdAllocator;
                  // let panelInfo = {pipes:[]};
                  // panelInfo["metaData"] = recommendedImage;
                  // // 추가되는 이미지에 대해선 자막을 없앰
                  // panelInfo.metaData.speechbubbles=[];
                  // panelInfo["imageCSS"] = {
                  // "rotation": 0, // (unit: deg)
                  // "width": that.props.panelWidth, // default width
                  // "height": that.props.panelHeigh ? that.props.panelHeigh : that.props.panelWidth / aspectRatio,
                  // "positionX": that.props.panelPositionX, //default position x
                  // "positionY": that.props.panelPositionY, //default position y
                  // };
                  // panelInfo["imageUrls"]=[...recommendedImage.url];
                  // panelInfo["cropPoints"]=[0,1,0,1]; //x1,x2,y1,y2 (in percent)console.log(newPanelId, panelInfo);

                  // this.props._postPanel(newPanelId, panelInfo, that.props.index);

                  // //판넬 개수 추가
                  // //해당 index page 판넬 리스트에 판넬 아이디 추가
              }}/>
          )
          })

          var addPanelContents = (
            <div
            style={{
              width: "100%",
              height: "100%",
            }}>
            <div className="windowHeader"
            style={{
              width: "100%",
              height:"10%",
            }}>

            </div>

            <div className="add-panel-option"
            style={{
              position:"block",
              width:"100%",
              height:"80%",
              overflow: "scroll",
            }}>
            {recommendedElements ?
              recommendedElements.map((recommendedElement)=>{
              return(
                recommendedElement
              )
            }) : null
            }
            </div>

            <div className="windowFooter">
            <Button onClick={(e)=>{
              that.props._closeWindow();
            }}>Close</Button>
            </div>

            </div>
          );
          that.props._addPanelWinodowOn(addPanelContents);

        // that.props._addPanelWinodowOn(addPanelContents);
        })
    })
  }

  componentDidMount(){
  }

  render(){
    var that = this;


    return(
      <div className="page-decorator"
        style={{
          margin:"auto 20",
          width:"50px",
          height:"250px",//this.props.page.height,
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-around",
      }}>

        <button ref="addPanelButton" className="add-panel-button btn glyphicon glyphicon-picture" type="button"
          style={{
            marginTop:"40px",
            width:"50px",
            height:"50px",
            fontSize:"25px",
            opacity: '0.5',
            backgroundColor: "rgba(0, 0, 0, 0)",
          }}
          onClick={this.onClickAddPanel.bind(this)}
        />

        {/*
            <button className="add-speechbubble-button btn glyphicon glyphicon-comment" type="button"
        style={{
        margin:"-40 0",
        width:"50px",
        height:"50px",
        fontSize:"25px",
        opacity: '0.5',
        backgroundColor: "rgba(0, 0, 0, 0)",
        }}
        onClick={(e)=>{
            console.log($("#panel-"+that.props.index+":nth-child(1)"));
            html2canvas($("header"), {
                onrendered: function(canvas) {
                    var theCanvas = canvas;
                    document.getElementById("page-"+that.props.index).appendChild(canvas);
                    console.log(theCanvas);
                    var imageData = theCanvas.toDataURL("image/png");//.replace("image/png", "image/octet-stream");
                    theCanvas.toDataURL("image/png");
                    window.open(theCanvas);
                    console.log(imageData);
                    var formData = new FormData();
                    formData.append('file', imageData);


                    $.ajax({
                    type : "POST",
                    url : "http://192.168.161.52:5000/api/worker/20170322.173217.352638.worker/upload" ,
                    data : imageData,
                    processData : false,
                    contentType: 'image/png',
                    success : function(data) {
                        console.log(data);
                    },
                    error : function(request, status, error) {
                        console.log(request, status, error);
                    }
                    });
                }
            });
        }} >

        </button>
        */}

        <button className="remove-page-button btn glyphicon glyphicon-trash"
          type="button"
          style={{
            marginBottom:"40px",
            width:"50px",
            height:"50px",
            fontSize:"25px",
            opacity: '0.5',
            backgroundColor: "rgba(0, 0, 0, 0)",
          }}
          onClick={this.onClickRemoveButton.bind(this)}
        />

      </div>
    )
  }
}

// action
function removePage(pageIndex){
  return({
    type:"REMOVE_PAGE",
    pageIndex: pageIndex,
  })
}

function windowOnWith(addPanelContents){
  return({
    type: "OPEN_WINDOW",
    windowContents: addPanelContents,
  })
}

function closeWindow(){
  return({ type: "CLOSE_WINDOW", })
}

function postPanel(newPanelId, panelInfo, pageIndex){
  return({
    type: "POST_PANEL",
    panelId: newPanelId,
    panelInfo: panelInfo,
    pageIndex: pageIndex,
  })
}

function updatePanel(panelId, panel){
    return({
    type: "UPDATE_PANEL",
    panelId: panelId,
    panelInfo: panel,
  })
}

// redux
function mapStateToProps(state){
  return({
    pages: state.vtoonData.pages,
    panels: state.vtoonData.panels,
    videoName: state.vtoonData.videoName,
    videoInfo: state.vtoonData.videoInfo,
    pageWidth: state.vtoonData.pageWidth,
    pageHeight: state.vtoonData.pageHeight,
    panelWidth: state.vtoonData.panelWidth,
    panelHeight: state.vtoonData.panelHeight,
    panelPositionX: state.vtoonData.panelPositionX,
    panelPositionY: state.vtoonData.panelPositionY,
    panelIdAllocator: state.vtoonData.panelIdAllocator,
    workerId: state.vtoonData.workerId,
  })
}

function mapDispatchToProps(dispatch){
  return({
    _removePage: (index) => { dispatch(removePage(index)) },
    _addPanelWinodowOn: (addPanelContents) => { dispatch(windowOnWith(addPanelContents)) },
    _closeWindow: () => { dispatch(closeWindow()) },
    _postPanel: (newPanelId, panelInfo, pageIndex) => { dispatch(postPanel(newPanelId, panelInfo, pageIndex)); },
    _updatePanel: (panelId, panel) => { dispatch(updatePanel(panelId, panel)); }
  })
}

PageDecorator = connect(mapStateToProps,mapDispatchToProps)(PageDecorator);
export default PageDecorator;

import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import {Popover,Modal,Tooltip,Button,OverlayTrigger,Collapse,Well} from 'react-bootstrap';
import $ from 'jquery';
import 'jquery-ui/themes/base/core.css';
import 'jquery-ui/ui/core';
import 'jquery-ui/themes/base/resizable.css';
import 'jquery-ui/ui/widgets/resizable';

import {SERVER_URL} from '../../constants'

import VideoBoard from './VideoBoard';
import EditorBoard from './EditorBoard';
import VideoLoader from './VideoLoader';
import VideoPublisher from './VideoPublisher';
import PanelWindow from './PanelWindow';

import ModalContainer from './Modal/ModalContainer';
import WindowContainer from './Window/WindowContainer';

class VideoView extends React.Component {
  constructor(props) { super(props); }

  componentDidMount() {
    // 우측의 side-bar의 크기를 조절 할 수 있게 jquery-ui 적용
    $(".side-bar").resizable({ handles: 'w' }).on("resize", function(event, ui){
        $(".side-bar").css("left",0)
      }
    )
  }

  render(){
    return (
      <div className="app-container"
        style={{
          display: "flex",
          flexDirection: "column",
          height: "100%"
      }}>

        <div className="header"
          style={{ flex:"0 0 64px"}}
        />

        <div className="app-body"
          style={{
            flex: "1",
            display: "flex",
            flexDirection: "row",
        }}>

          <EditorBoard />

          <div className="side-bar"
          style={{
            width: '300px',
            display: '"flex',
            flexDirection: '"column',
          }}>
            <VideoLoader />
            <VideoPublisher />
            <VideoBoard />
            <PanelWindow />
          </div>

          <WindowContainer contents={this.props.windowContents}/>
          <ModalContainer contents={this.props.modalContents}/>
        </div>

        <div className="footer"
          style={{ flex: '0 0 70px' }}
        />

      </div>
    );
  }
}

// action
function openModal(modalContents){
  return{
    type: "OPEN_MODAL",
    modalContents: modalContents,
  }
}

// redux
function mapStateToProps(state) {
    return {
      selectedPanelId: state.vtoonData.selectedPanelId,
      windowContents: state.vtoonData.windowContents,
      modalContents: state.vtoonData.modalContents,
    };
}

function mapDispatchToProps(dispatch) {
    return ({
      _openModal: (modalContents)=>{ dispatch(openModal(modalContents)) },
    });
}

VideoView = connect(mapStateToProps, mapDispatchToProps)(VideoView);

export default VideoView;

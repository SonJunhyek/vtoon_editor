import React from 'react';
import {connect} from 'react-redux';
import $ from 'jquery';

import 'jquery-ui/ui/core';
import 'jquery-ui/themes/base/core.css';
import 'jquery-ui/ui/widgets/draggable';
import 'jquery-ui/themes/base/draggable.css';
import 'jquery-ui/themes/base/resizable.css';
import 'jquery-ui/ui/widgets/resizable';

import Page from './Page';
import PageDecorator from './PageDecorator';

class EditorBoard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      originX: null,
      originY: null,
      mouseX: null,
      mouseY: null,
      ballonX: 100,
      ballonY: 100,
      zoomScale: 1,
      blockerHeight: "100%",
    };
    this.rescaleEditorBoard = this.rescaleEditorBoard.bind(this);

  }

  componentDidMount() {
    var that = this;
    $(".zoomTarget").prop("data-targetsize", "0.45");
    $(".zoomTarget").prop("data-duration", "600");
  }

  handleStart(e, ui) {
    e.stopPropagation();
  }
  handleStop(e, ui) {
    console.log("this", e.target.style);
    e.stopPropagation();
  }
  handleDrag(e, ui) {
    e.stopPropagation();
  }

  ondragstart(e) {
    e.stopPropagation();
    var divs = document.createElement("div");
    e.dataTransfer.setDragImage(divs, 0, 0);
    this.setState({
      originX: e.pageX,
      originY: e.pageY,
    });
  }

  ondrag(e) {
    e.preventDefault();
    var difX = e.pageX - this.state.originX;
    var difY = e.pageY - this.state.originY;
    e.currentTarget.parentNode.style.transform += "rotate(" + difX + "deg)";
    console.log(e.nativeEvent.offsetX, e.nativeEvent.offsetY);
  }

  rescaleEditorBoard(rate) {
    var that = this;
    that.setState({
      zoomScale: that.state.zoomScale * rate
    }, () => {
      var editorBoards = document.getElementById("editor-boards");
      var panelContainment = $("#page-" + this.state.id)

      var dragFix = (event, ui) => {
        var contWidth = panelContainment.width(),
          contHeight = panelContainment.height();
        ui.position.left = Math.max(0, Math.min(ui.position.left / that.state.zoomScale, contWidth - ui.helper.width()));
        ui.position.top = Math.max(0, Math.min(ui.position.top / that.state.zoomScale, contHeight - ui.helper.height()));
      }
      editorBoards.style.transformOrigin = editorBoards.style.offsetX + " " + editorBoards.style.offsetY;
      //      editorBoards.style.transform="scale("+that.state.zoomScale+")";
      editorBoards.style.zoom = that.state.zoomScale;

      console.log("log", this.refs.draggableBoard);

      $("#panel-" + this.state.id).draggable({
        containment: "parent",
        drag: dragFix
      });
      $("#left-bottom-" + this.state.id).draggable({
        containment: "#panel-" + this.state.id,
        scroll: false
      });
    })
  }

  onClickZoomIn(e) {
    this.rescaleEditorBoard(1 / 0.95);
    console.log($("#editor-boards").width());
    console.log($("#editor-boards"));
  }

  onClickZoomOut(e) {
    this.rescaleEditorBoard(0.95);
  }

  render() {
    var that = this;
    var pages = this.props.pages;
    var panels = this.props.panels;
    var speechBubbles = this.props.speechBubbles;
    return (
      <div id = "background"
      style = {
        {
          position: "relative",
          backgroundColor: "grey",
          flex: "1",
          overflow: "scroll",
        }
      } >

      <div id = "zoom-in-editor-board"
      className = "glyphicon glyphicon-zoom-in"
      style = {
        {
          fontSize: "30px",
          position: "fixed",
          top: "100px",
          left: "20px",
          zIndex: 2,
        }
      }
      onClick = {
        this.onClickZoomIn.bind(this)
      } > </div>

      <div  id = "zoom-in-editor-board"
      className = "glyphicon glyphicon-zoom-out"
      style = {
        {
          fontSize: "30px",
          position: "fixed",
          top: "150px",
          left: "20px",
          zIndex: 2,
        }
      }
      onClick = {
        this.onClickZoomOut.bind(this)
      }
      />
      <div id = "editor-boards"
      className = "zoomContainer"
      onDragOver = {
        (e) => {
          e.preventDefault();
        }
      }
      style = {
        {
          position: "relative",
          width: "690px",
          overflow: "visible",
          margin: "0 auto",
        }
      } >
      { pages ? pages.map((page, index) => {
          return (
            <div key={index}>
              <div
                style={{
                    display: "flex",
                    flexDirection: "row",
                    margin: "15 auto",
                }}
              >
                <Page
                  page = {page}
                  index = {index}
                />
                <PageDecorator
                  page = {page}
                  index = {index}
                />
              </div>
              <div className = "inter-section"
                style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    marginRight: "90px",
                }}
              >
                <button
                  className="add-page-button btn glyphicon glyphicon-plus"
                  type="button"
                  style = {{
                    width: "40px",
                    height: "40px",
                    borderRadius: '50%',
                    backgroundColor: 'white',
                    opacity: '0.7',
                  }}
                  onClick = {
                    (e) => {
                      var pageInfo = {
                        "id": null,
                        "width": 600,
                        "height": 350,
                        "panels": [],
                        "speechBubbles": [],
                      };
                      that.props._addPage(pageInfo, index);
                    }
                  }
                />
                <button
                  className = "merge-page-button btn glyphicon glyphicon-link"
                  type = "button"
                  style = {
                    {
                      width: "40px",
                      height: "40px",
                      borderRadius: '50%',
                      backgroundColor: 'white',
                      opacity: '0.7',
                    }
                  }
                  onClick = {
                    (e) => {
                      var newPage = Object.assign({}, pages[index]);
                      var mergedPage = Object.assign({}, pages[index + 1]);
                      var pageHeight = newPage.height;
                      // 합쳐지는 페이지의 요소들의 positionY를 이전 page의 height만큼 증가시키기
                      mergedPage.panels.map((panelId) => {
                        let panelInfo = Object.assign({}, that.props.panels[panelId])
                        panelInfo.imageCSS.positionY += pageHeight;
                        that.props._updatePanel(panelId, panelInfo);
                      })
                      mergedPage.speechBubbles.map((speechBubbleId) => {
                        let speechBubble = Object.assign({}, that.props.speechBubbles[speechBubbleId])
                        speechBubble.positionY += pageHeight;
                        that.props._postSpeechBubble(speechBubble, speechBubbleId);
                      })
                      that.props._mergePage(newPage, mergedPage, index);
                    }
                  }
                />
              </div>
            </div>
          )
        }) : null
      }

      </div>
      </div>
    )
  }
}

// action
function mergePage(newPage, mergedPage, index) {
  return ({
    type: "MERGE_PAGES",
    newPage,
    mergedPage,
    index,
  })
}

function updatePanel(panelId, panelInfo) {
  return ({
    type: "UPDATE_PANEL",
    panelInfo: panelInfo,
    panelId: panelId,
  })
}

function postSpeechBubble(speechBubble, speechBubbleId) {
  return ({
    type: "POST_SPEECH_BUBBLE",
    speechBubble,
    speechBubbleId,
  })
}

function openModal(content) {
  return ({
    type: "OPEN_MODAL",
    content: content,
  })
}

function addPage(pageInfo, index) {
  return ({
    type: "ADD_PAGE",
    index: index,
    pageInfo: pageInfo,
  })
}

// redux
function mapStateToProps(state) {
  return ({
    pages: state.vtoonData.pages,
    panels: state.vtoonData.panels,
    speechBubbles: state.vtoonData.speechBubbles,
  });
}

function mapDispatchToProps(dispatch) {
  return {
    _postSpeechBubble: (speechBubble, speechBubbleId) => {
      dispatch(postSpeechBubble(speechBubble, speechBubbleId));
    },
    _updatePanel: (panelId, panelInfo) => {
      dispatch(updatePanel(panelId, panelInfo));
    },
    _mergePage: (newPage, mergedPage, index) => {
      dispatch(mergePage(newPage, mergedPage, index));
    },
    _openModal: (content) => {
      dispatch(openModal(content))
    },
    _addPage: (pageInfo, index) => {
      dispatch(addPage(pageInfo, index));
    },
  }
}

EditorBoard = connect(mapStateToProps, mapDispatchToProps)(EditorBoard);
export default EditorBoard;

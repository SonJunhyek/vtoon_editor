import React from 'react';
import { connect } from 'react-redux';

import $ from 'jquery';
import 'jquery-ui/themes/base/core.css';
import 'jquery-ui/ui/core';
import 'jquery-ui/themes/base/resizable.css';
import 'jquery-ui/ui/widgets/resizable';

import Panel from './Panel';
import SpeechBubble from './SpeechBubble';

class Page extends React.Component {
  constructor(props) { super(props);}

  componentDidMount(){
    var that = this;
   $("#"+that.props.page.id).resizable({
     handles: 's',
     stop: (event, ui)=>{
       that.props._resizePage(that.props.index, ui.size.height);
     }
    })
  }

  render() {
    var that = this;
    var page=this.props.page;
    var index = this.props.index;
    return (
      <div id={page.id}
        className = "zoomTarget"
        style = {{
          position: "relative",
          width: page.width,
          height: page.height,
          backgroundColor: "white",
        }}
      >

      {
        page.panels ?
        page.panels.map((panelKey, index) => {
          var panel = that.props.panels[panelKey];
          return (
            < Panel
              panel={panel}
              id={panelKey}
              key={index}
            />)
          }
        ) : null
      }

      {/*
      page.speechBubbles ? page.speechBubbles.map((speechBubbleKey, index) => {
          var speechBubble = that.props.speechBubbles[speechBubbleKey];
          return ( <
              SpeechBubble speechBubble = {
                  speechBubble
              }
              id = {
                  speechBubbleKey
              }
              />
          )
      }) : null
      */}
      </div>
    )
  }
}

// action
function postPanelId(panelId) {
  return ({
    type: "POST_SELECTED_PANEL",
    selectedPanelId: panelId,
  })
}

function resizePage(pageIndex, height) {
  return ({
    type: "RESIZE_PAGE",
    pageIndex: pageIndex,
    height: height,
  })
}


// redux
function mapStateToProps(state) {
  return ({
    pages: state.vtoonData.pages,
    panels: state.vtoonData.panels,
    speechBubbles: state.vtoonData.speechBubbles,
  });
}

function mapDispatchToProps(dispatch) {
  return {
    _onSelectPanel: (panelId) => {
      dispatch(postPanelId(panelId));
    },
    _resizePage: (pageIndex, height) => {
      dispatch(resizePage(pageIndex, height));
    },
  }
}

Page = connect(mapStateToProps, mapDispatchToProps)(Page);
export default Page;

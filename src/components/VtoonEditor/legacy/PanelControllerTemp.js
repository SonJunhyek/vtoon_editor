export default class PanelController extends React.Component {
    constructor(props) { super(props); }

    render() {
      return(
        <div>
          <span
            style = {{
                position: "absolute",
                top: "-10px",
                right: "-10px",
                width: "10px",
                height: "10px",
                backgroundColor: "tomato",
              }}
            draggable = "true"
            onMouseDown = {
              (e) => {
                e.stopPropagation();
              }
            }
            onDragStart = {
              (e) => {
                e.stopPropagation();
                this.setState({
                  originX: e.pageX,
                  originY: e.pageY
                });
              }
            }
            onDrag = {
              (e) => {
                e.preventDefault();
                var difX = e.pageX - that.state.originX;
                var difY = e.pageX - that.state.originY;
                e.currentTarget.parentNode.style.transform = "rotate(" + difX + "deg)";
              }
            }>
            </span>

            <span className = "scale-controller"
              style = {
                {
                  position: "absolute",
                  bottom: "-10px",
                  right: "-10px",
                  width: "10px",
                  height: "10px",
                  backgroundColor: "tomato",
                }
              }
              draggable = "true"
              onClick = {
                (e) => { console.log("on click", e.currentTarget.parentNode) }
              }
              onMouseDown = {
                (e) => { e.stopPropagation(); }
              }
              onDragStart = {
                (e) => {
                  e.stopPropagation();
                  this.setState({
                    originX: e.pageX,
                    originY: e.pageY
                  });
                }
              }
            onDrag = {
                (e) => {
                    e.stopPropagation();
                    e.preventDefault();
                    var difX = e.pageX - that.state.originX;
                    var difY = e.pageY - that.state.originY;
                    // that.setState({imageCSS.widththis.state.imageCSS.width})
                    e.currentTarget.parentNode.style.width = that.state.imageCSS.width + difX;
                    e.currentTarget.parentNode.style.height = that.state.imageCSS.height + difY;
                }
            }
            onDragEnd = {
                (e) => {
                    e.preventDefault();
                    var temp = this.state.imageCSS;
                    temp["width"] = e.currentTarget.parentNode.style.width;
                    that.setState({
                        imgaeInfo: temp
                    });
            }}>
            </span>

          <span id = {"left-bottom-" + that.state.id}
          style = {
              {
                  position: "absolute",
                  top: that.props.panel.imageCSS.height - 10,
                  left: 0,
                  width: "10px",
                  height: "10px",
                  backgroundColor: "SlateBlue",
                  backgroundPosition: "-5px -5px",
              }
          }
          draggable = "true"
          onMouseDown = {
              (e) => {
                  e.stopPropagation();
                  console.log("down");
              }
          }
          onDragStart = {
              (e) => {
                  e.stopPropagation();
                  console.log("dragstart");
              }
          }
          onMouseMove = {
              (e) => {
                  that.setState({
                      y4: e.currentTarget.style.top.split('px')[0],
                      x4: e.currentTarget.style.left.split('px')[0],
                  });
              }
          }
          onDrag = {
              (e) => {
                  e.preventDefault();
                  console.log("this", e.currentTarget.style.x, e.currentTarget.style.y);
                  this.setState({
                      x4: e.currentTarget.style.x,
                      y4: e.currentTarget.style.y,
                  });
              }
          }
          onDragEnd = {
              (e) => {
                  e.preventDefault();
                  var temp = this.state.imageCSS["width"];
                  temp["width"] = e.currentTarget.parentNode.style.width;
                  that.setState({
                      imgaeInfo: temp
                  });
              }
          }>
          </span>
        </div>
    }
}

import React from 'react';
import {connect} from 'react-redux';
import {Tooltip,OverlayTrigger} from 'react-bootstrap';
import $ from 'jquery';

// actions
function loadImageToEditor(panel, pageIndex){
  return{
    type: "LOAD_IMAGE_TO_EDITOR",
    panel: panel,
    pageIndex: pageNum,
  }
}

// connect to store
function mapStateToProps(state){
  return {
    rawImages: state.vtoonData.rawImages,
    videoInfo: state.vtoonData.videoInfo,
    pages: state.vtoonData.pages,
  }
}

function mapDispatchToProps(dispatch){
  return {
    _loadImageToEditor: (panel)=>{
      dispatch(loadImageToEditor(panel));
    },
  }
}

@connect(mapStateToProps, mapDispatchToProps)
export default class RawImageBoard extends React.Component {

  constructor(props){
    super(props);
  }

  onClickRawImage(e){
    var aspectRatio = this.props.videoInfo.width/this.props.videoInfo.height;
    var that = this;
    var i = parseInt(e.target.id.split('-')[2]);// raw image's index
    var panel = {};
    panel["metaData"] = this.props.rawImages[i];
    panel["id"] = this.props.rawImages[i].id;
    console.log("panel : ", panel);
    // TODO. Change raw image to panel (add more info refer structure.js)
    panel["imageInfo"] = {
      "rotation": 0, // (unit: deg)
      "width": 400, // (unit: px)
      "height": 400 / aspectRatio,
      "positionX": 100,
      "positionY": 60,
    };
    that.props._loadImageToEditor(panel);
    console.log("clientHeight",$("#editor-boards")[0].clientHeight);

    // scroll to bottom
    $("#background").animate({ scrollTop: $("#editor-boards")[0].clientHeight }, "slow");

  }

  render(){
    var that=this;
    var rawImages = this.props.rawImages;

    return (
      <div
        style={{
          flex:"1",
          height:"inherit",
          width:"20%",
          overflow:"scroll"}}>
          { rawImages? rawImages.map((image,index)=>{
            if( index%15 == 0){
            var caption = image.caption?image.caption.contents.contents:null;
            var tooltip = (
              <Tooltip id={"tooltip-"+index}>
                <p> {caption} </p>
              </Tooltip>
            );

            var Img =
                <img
                id={"raw-img-"+index}
                style={{width:"100%"}}
                src={image.url}
                alt="..."
                onClick={that.onClickRawImage.bind(that)}></img>;

            return (
              <OverlayTrigger overlay={tooltip}>
                {Img}
              </OverlayTrigger>)
            }
            return null
          }) : null }

      </div>)
  }
}

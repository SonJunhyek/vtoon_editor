import React from 'react';
import {connect} from 'react-redux';
import {Tooltip,OverlayTrigger} from 'react-bootstrap';
import axios from 'axios';

// constants
import {SERVER_URL, SERVER_IP} from '../../constants'

// actions
function loadRawImages(rawImages){
  return ({
    type:"LOAD_RAW_IMAGES",
    rawImages: rawImages,
  });
}

// connect to store
function mapStateToProps(state){
  return({
    videoName: state.vtoonData.videoName,
    recommendImages: state.vtoonData.recommendImages,
  })
}

function mapDispatchToProps(dispatch){
  return({
    _loadRawImages:(rawImages) => {
      dispatch(loadRawImages(rawImages));
    },
  })
}

@connect(mapStateToProps,mapDispatchToProps)
export default class RecommendImageBoard extends React.Component {

  constructor(props){
    super(props);
  }

  onCLickRecommendImage(e){
    var that =this;
    var i = parseInt(e.target.id.split('-')[2]);// recommend image's index
    var start;
    var end;
    var frameOfPreviousImage = this.props.recommendImages[i-1].no;
    var frameOfNextImage = this.props.recommendImages[i+1].no;
    var API = "http://"+SERVER_URL+":"+SERVER_IP +"/api/meta/";
    var video = document.getElementById("video");

    start = frameOfPreviousImage;
    end = frameOfNextImage;
    API = API + this.props.videoName +
    "?start="+ start +
    "&end="+ end +
    "&unit=frame" + "&key=false" ;

    // 선택한 이미지 앞의 이미지 시점부터 영상 플레이
    video.currentTime = this.props.recommendImages[i-1].time;
    video.play();
    console.log(API);

    // (데이터) store.vtoonData.rawImages에 클릭 한 장면 전,후 추천된 장면 사이의 데이터를 불러온다
    axios.get(API).then((res)=>{
      var rawImagesJson = res.data;
      var rawImageArray = [];
      var rawImageFrameNums = Object.keys(res.data);

      rawImageFrameNums.map((frameNum)=>{ //rawImage data reforming ( from json to array )
        var rawImage = rawImagesJson[frameNum];
        rawImage["id"]= frameNum;
        rawImageArray.push(rawImage);
      })
      console.log("rawImageArray",rawImageArray);
      that.props._loadRawImages(rawImageArray);
    })
    // (애니메이션) 변동사항이 있을 시, RecommendImageBoard 가 줄어들며 우측에 rawImage board가 뜬다.
    // (event)
  }

  render(){
    var that = this;
    var recommendImages = this.props.recommendImages;
    return (
      <div
        style={{
          flex:"3",
          height: "inherit",
          overflow:"scroll",
        }}>
        { recommendImages ? recommendImages.map((image, index)=>{

          var tooltip = (
            <Tooltip id={"tooltip-"+index}> {image.caption.map((caption,index)=>{
              return <p key={index}> {caption} </p>;
            })} </Tooltip>
          );

          var Img = <img
              id={"recommend-img-"+index}
              style={{width:"100%"}}
              src={image.url}
              alt="..."
              onClick={that.onCLickRecommendImage.bind(that)}></img>;

          if(image.caption.length==0) {
            return Img
          } else {
            return (
              <OverlayTrigger overlay={tooltip}>
                {Img}
              </OverlayTrigger>
            );
          }
        }) : null }
      </div>
    )
  }
}

import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import {Popover,Modal,Tooltip,Button,OverlayTrigger,Collapse,Well} from 'react-bootstrap';

// constants
import {SERVER_URL} from '../../constants'

class VideoLoader extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      showModal: false,

      // video
      selectableVideoOptions:[],
      videoName:null,
      videoInfo:null,

      // options that API request would have
      framesPercentage:0.001,
      skipFrames: null,
      fromFrame:1,
      toFrame:20000,
      metaType: 2,
      preset: null,
      detail: 1,
    };
  }

  componentDidMount(){
    var that = this;
    var API = SERVER_URL+"/api/meta";

    axios.get(API).then((res)=>{
      that.setState({selectableVideoOptions:res.data});
    });

  }

  selectVideo(e){
    var that = this;
    var API = SERVER_URL+"/api/meta";
    var target = e.target;
    var videoName = target.value;
    var videoInfo;
    API = API + "/" + videoName;

    axios.get(API).then((res)=>{
      videoInfo = res.data;
      that.setState({
        videoName:videoName,
        videoInfo:videoInfo,
      },()=>console.log(that.state));
    })
  }

  //Modal controll
  close() { this.setState({ showModal: false }); }

  open() { this.setState({ showModal: true }); }

  // load images of selected video
  load() {
    var that = this;
    var nbframe = this.state.videoInfo.nbframe; // the number of frames
    var panels = {};
    var speechBubbles = {};
    var pages = [];
    var WORKER_API = SERVER_URL+"/api/worker";
    var workerId;
    // initialize store for loading other video
    this.props._initStore();

    // save new video name and video info in store
    this.props._postVideo(this.state.videoName,this.state.videoInfo);

    this.setState({ showModal: false });

    // 워커 생성 : [POST] SERVER_URL+"/api/worker?metaname="" & Body(없어도 됨)
    axios.post(WORKER_API+"?metaname="+that.state.videoName,{}).then(
    (res)=>{
      workerId = res.data;
      console.log("workerId", workerId);
      //workerId store에 업로드
      that.props._postWorkerId(workerId);

    // 응답 받은 워커 아이디 통해 Recommend 요청 : [GET] SERVER_URL+/api/worker/{taskid}/recommend
      let RECOMMEND_API = WORKER_API+"/"+workerId+"/recommend?";

      if(that.state.framesPercentage)   RECOMMEND_API = RECOMMEND_API + "percentOfFrames=" + that.state.framesPercentage;
      if(that.state.skipFrames)         RECOMMEND_API = RECOMMEND_API + "&skipFrames="     + that.state.skipFrames;
      if(that.state.fromFrame)          RECOMMEND_API = RECOMMEND_API + "&fromFrame="      + that.state.fromFrame;
      if(that.state.toFrame)            RECOMMEND_API = RECOMMEND_API + "&toFrame="        + that.state.toFrame;
      if(that.state.metaType)           RECOMMEND_API = RECOMMEND_API + "&metaType="       + that.state.metaType;
      if(that.state.preset)             RECOMMEND_API = RECOMMEND_API + "&preset="         + that.state.preset;
      if(that.state.detail)             RECOMMEND_API = RECOMMEND_API + "&detail="         + that.state.detail;

      axios.get(RECOMMEND_API).then(
      (res)=>{
        // worker로 image 이동 [PUT] SERVER_URL+/api/worker/{taskid}/recommend?store=1
        axios.put(SERVER_URL+"/api/worker/"+workerId+"/recommend?store=1")
        .then((res)=>{
          var images = res.data.recommended;
          var aspectRatio = that.props.videoInfo.width/that.props.videoInfo.height;

          images.map((image, index)=>{
            //make panel and save it in panel
            var panelInfo = {pipes:[]};
            panelInfo["metaData"] = image;
            panelInfo["imageCSS"] = {
              "rotation": 0, // (unit: deg)
              "width": that.props.panelWidth, // default width
              "height": that.props.panelHeigh ? that.props.panelHeigh : that.props.panelWidth / aspectRatio,
              "positionX": that.props.panelPositionX, //default position x
              "positionY": that.props.panelPositionY, //default position y
            };
            panelInfo["imageUrls"]=[...image.urls];
            panelInfo["cropPoints"]=[0,1,0,1]; //x1x2,y1,y2 (in percent)
            panels["panel-"+index] = panelInfo;

            //make speechBubble and save it in store
            var speechBubbleInfo = {};
            speechBubbleInfo["positionX"]= 20;
            speechBubbleInfo["positionY"]= 30;
            speechBubbleInfo["scripts"]=image.caption; //array
            speechBubbles["speechBubble-"+index] = speechBubbleInfo;

            //make page and save it in store
            var pageInfo = {
                "id":null,
                "width":600, //this.props.store.pageWidth,
                "height":300, //this.props.store.pageHeight,
                "panels":[],
                "speechBubbles":[],
                "startTime":image.time,
                "endTime":image.time,
                "startFrame":image.no,
                "endFrame":image.no,
              };

              pageInfo.panels.push("panel-"+index);
              pageInfo.speechBubbles.push("speechBubble-"+index);
              pageInfo.id="page-"+index;
              pages.push(pageInfo);
          })

          that.props._postAll(panels, speechBubbles, pages, pages.length-1);
          console.log("put server data to worker's stage", res);

        }).catch( (error)=>{
          console.log(error);
        })
      }).catch( (error) => {
        console.log(error);
      });
    }).catch( (error) => {
      console.log(error);
    });
  }

  // input[type="text"] 의 텍스트 변경이벤트 state 처리
  onChagneText(e){
    switch(e.target.id){
      case "framesPercentage":
          this.setState({ framesPercentage: e.target.value });
          break;
      case "skipFrames":
          this.setState({ skipFrames: e.target.value });
          break;
    }
  }

  render(){
    var that = this;
    return(
      <div style={{width:"100%"}}>
        <button
          className="btn"
          style= {{width:"100%"}}
          onClick={this.open.bind(this)}
        >
          Load
        </button>

        <Modal show={this.state.showModal} style={{marginTop:'64px'}} onHide={this.close.bind(this)}>
          <Modal.Header closeButton >
            <Modal.Title>Load Video</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>
              <select onClick={this.selectVideo.bind(this)}>
                {this.state.selectableVideoOptions.map(function(videoName, index){
                  return (
                    <option
                      value={videoName}
                      key={index}> {videoName} </option>
                  )
                })}
              </select>

              <div>
                <div style={{width:"inherit", overflow:"auto", paddingTop:"20px"}}>
                  <p> 비디오 정보 </p>
                  <p> 프레임 수 : {this.state.videoInfo ? this.state.videoInfo.nbframe + " 장" : ""} </p>
                  <p> 영상 길이 : {this.state.videoInfo ? Math.round(this.state.videoInfo.duration) + " 초" :""} </p>
                  <p> 영상 크기 : {this.state.videoInfo ? this.state.videoInfo.width+" * " : ""}
                  {this.state.videoInfo ? this.state.videoInfo.height : ""} </p>
                </div>

                <p> Percent of Frames :
                  <input
                    id="framesPercentage"
                    placeholder="float"
                    type="text"
                    value={this.state.framesPercentage}
                    onChange={this.onChagneText.bind(this)} /> </p>

                <p> Skip frames :
                  <input
                    id="skipFrames"
                    placeholder="int"
                    type="text"
                    value={this.state.skipFrame}
                    onChange={this.onChagneText.bind(this)} />
                    (큰변화값이 연속적으로 들어올 경우 무시 할 프레임 수) </p>
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.load.bind(this)}>Load</Button>
            <Button onClick={this.close.bind(this)}>Close</Button>
          </Modal.Footer>
        </Modal>

      </div>

    )
  }
}

// actions
function postVideo(videoName,videoInfo) {
  return ({
    type: "POST_VIDEO",
    videoName:videoName,
    videoInfo:videoInfo,
  });
}

function postAll(panels, speechBubbles, pages, lastIndex){
  return({
    type: "POST_ALL",
    panels: panels,
    speechBubbles: speechBubbles,
    pages: pages,
    lastIndex: lastIndex,
  })
}

function initStore(){
  return({
    type: "INIT_STORE",
  })
}

function postWorkerId(workerId){
  return({
    type: "POST_WORKER_ID",
    workerId: workerId,
  })
}

// redux
function mapStateToProps(state){
    return({
        store: state.vtoonData,
        pages: state.vtoonData.pages,
        videoName: state.vtoonData.videoName,
        videoInfo: state.vtoonData.videoInfo,
        pageWidth: state.vtoonData.pageWidth,
        pageHeight: state.vtoonData.pageHeight,
        panelWidth: state.vtoonData.panelWidth,
        panelHeight: state.vtoonData.panelHeight,
        panelPositionX: state.vtoonData.panelPositionX,
        panelPositionY: state.vtoonData.panelPositionY,
    })
}

var mapDispatchToProps = (dispatch) => {
  return {
    _postVideo: (videoName,videoInfo) => {
      dispatch(postVideo(videoName,videoInfo))
    },
    _postAll: (panels, speechBubbles, pages, lastIndex) => {
      dispatch(postAll(panels, speechBubbles, pages, lastIndex));
    } ,
    _initStore: () => {
      dispatch(initStore());
    },
    _postWorkerId: (workerId) => {
      dispatch(postWorkerId(workerId));
    }
  }
}

VideoLoader = connect(mapStateToProps, mapDispatchToProps)(VideoLoader);
export default VideoLoader;

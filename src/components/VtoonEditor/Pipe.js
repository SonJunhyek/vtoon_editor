import  React from "react";
import { connect } from 'react-redux';
import axios from 'axios';
import { Popover,Modal,Tooltip,Button,OverlayTrigger,Collapse,Well } from 'react-bootstrap';

// constants
import { SERVER_URL } from '../../constants'

class Pipe extends React.Component {
  constructor(props){
    super(props);
    this.state={
      name: "",
      desc: "",
      args: {},
      argsJson: {},

      open:false,
      selectablePipeList:[],
      previousPipe:"",
      selectedPipe:"",

      pipedImageUrl:"",
    };
  }

  componentWillReceiveProps(){
      document.getElementsByClassName("piped-image")[0].src = "";
  }

  componentDidMount(){
    var that=this;
    // 사용 가능한 필터 목록 ajax로 요청을
    axios.get(SERVER_URL+"/api/pipeline").then((res)=>{
      var selectablePipeListTemp = [];
      res.data.pipes.map((pipe,index)=>{
        selectablePipeListTemp.push({name:pipe,key:index});
      });
      that.setState({
        name: selectablePipeListTemp[0].name,
        selectablePipeList: selectablePipeListTemp,
        previousPipe: selectablePipeListTemp[0].name,
        selectedPipe: selectablePipeListTemp[0].name
      });
      console.log(that.state);
    });

  }

  componentDidUpdate(){
      document.getElementById("select-pipe").value = this.state.name;
      console.log("tt",document.getElementsByClassName("piped-image"));
  }

  selectPipe(e){
    var that=this;
    var target = e.target;
    var argsTemp = this.state.args;
    // AJAX 요청을 통한 선택 된 필터의 HELP 받아오기
    axios.get(SERVER_URL+"/api/pipeline?pipeline_name="+ target.value).then((res)=>{
      var previousPipe = that.state.selectedPipe;
      console.log("axios data", res.data);
      // TODO 받아온 JSON정보를 바탕으로 pipeInfo를 만드는 Form format 작성하기
      if(target.value!=that.state.name){ //다른 pipe 고를 시 초기화
        argsTemp={};
      }
      that.setState({
        name:target.value,
        desc: JSON.stringify(res.data.desc),
        args: argsTemp,
        argsJson: res.data.args,
        open: false, // 파라미터가 의미가 없어졌기에 꺼둠
        previousPipe:previousPipe,
        selectedPipe:target.value
      });
      console.log("state",that.state);
    }).catch((error)=>{
        var previousPipe = that.state.selectedPipe;

        that.setState({
            name:target.value,
            previousPipe:previousPipe,
            selectedPipe:target.value
        });
    });
  }

//update or make new pipe on pipeList
  updatePipe(){
    var pipe = {
                'pipeName':this.state.name,
                'pipeInfo':
                  {
                    "sources": [],
                    "args": this.state.args,
                    "type": this.state.name
                  }
                }

    this.setState({
      name: document.getElementById("select-pipe").value,
      args:this.state.args,
      open:false
    });
  }

  cancelPipeSetter(){
    this.setState({open:false, selectedPipe:this.state.previousPipe});
    //TODO selection tag의 value를 previousPipe로 바꿔야함
    document.getElementById("select-pipe").value = this.state.previousPipe;
  }

  inputChange(e){
    var target = e.target;
    var newArgs = this.state.args;
    newArgs[target.name]=target.value;
    this.setState({
      args:newArgs
    });
  }

  requestPipe(){
    var that=this;
    var requestBody={
        "Pipe_One": {
            "sources": ["frame."+that.props.selectedPanel.metaData.frame],
            "args": this.state.args,
            "type": this.state.selectedPipe}
    }

    axios.post(SERVER_URL+"/api/pipeline?"+"pipeline_name="+"untitled"+ "&test=true&meta_name="+that.props.videoName+"&overwrite=true",requestBody
    ).then((res)=>{
    console.log(res,"res");
        // pipe 처리 완료
        that.setState({
          pipedImageUrl: res.data.urllist[0],
          open: false,
        });
    });
  }

  applyPipe(){
    var panel = Object.assign({},this.props.selectedPanel);
    // 해당 판넬에 접근하여 imageUrls에 해당 이미지 정보 추가
    panel.imageUrls.push(this.state.pipedImageUrl);
    // 적용된 파이프 정보 추가
    panel.pipes.push(this.state.selectedPipe);
    this.props._updatePanel(this.props.selectedPanelId, panel);
  }

  render() {
    var that=this;
    return (
      <div style={{
          position: 'relative',
          width: '100%',
          flex:'1 1 auto',
          padding: 10,
          backgroundColor: '#FFF',
          borderBottom: '1px solid #EFEFEF',
          boxSizing: 'border-box',
          WebkitUserSelect: 'none',
          overflow: 'scroll',
      }}>

        <img className="original-image"
            src={that.props.selectedPanel.imageUrls[0]}
            style={{
            width: "100%"}}/>
        <div
            style={{display:"flex", }}>
            <select id="select-pipe" className="form-control" onClick={this.selectPipe.bind(this)}
                style={{
                    flex:"3",
                    width:'60%',
                    display:"inline-block",
                    height:'35px'
                }}>
                {
                that.state.selectablePipeList.map((pipe,index)=>{
                return <option  className={index}
                                key={index}
                                name={pipe.name}
                                value={pipe.name} > {pipe.name} </option>
                })}
            </select>

            <button id="pipe-request-button" type="button" className="btn glyphicon glyphicon-play"
                style={{
                width:"40px",
                height:"40px",
                borderRadius:'50%',
                backgroundColor:'white',
                flex:"1",
                opacity: '0.7'
                }}
                onClick={this.requestPipe.bind(this)} >
            </button>

            <button id="apply-pipe-button" type="button" className="btn glyphicon glyphicon-log-in"
                style={{
                width:"40px",
                height:"40px",
                borderRadius:'50%',
                backgroundColor:'white',
                flex:"1",
                opacity: '0.7',
                }}
                onClick={this.applyPipe.bind(this)} >
            </button>
        </div>

        <Collapse in={this.state.open}>
          <div style={{
            borderRadius: '5px',
            height:'400px',
            marginTop:'5px',
            overflow:'scroll'}}
            >
            <p>Description: {this.state.desc}</p>

            {that.state.argsJson? Object.keys(that.state.argsJson).map((argName,index)=>{
              var arg = that.state.argsJson[argName];
              if(arg.type=="string"){
                return (
                  <div key={index}>
                    <div>
                     {index}. {argName} ({arg.type}) :
                    </div>
                    <select
                      className="form-control"
                      name={argName}
                      onChange={this.inputChange.bind(this)}
                      style={{}}>
                      {
                        arg.items.map((item,index)=>{
                        return <option  className={index}
                                        key={index}
                                        name={item}
                                        value={item} > {item} </option>
                      })}
                    </select>
                    <div>
                      description : {arg.desc}
                    </div>
                  </div>
                )
              } else {
                  return (
                    <div key={index}>
                      <div>
                       {index}. {argName} ({arg.type}) :
                      </div>
                      <input
                        type="text"
                        name={argName}
                        placeholder={argName}
                        value={this.state.args[argName]}
                        onChange={this.inputChange.bind(this)}/>

                      <div>
                        description : {arg.desc}
                      </div>
                    </div>
                  )
                }
            }) : null}

            <button className="btn update" type="button"
              style={{
                height:'35px',
                fontSize:'100%',
                display:"inline-block"
              }} onClick={this.updatePipe.bind(this)}>
              update
            </button>

            <button className="btn cancel" type="button"
              style={{
                height:'35px',
                fontSize:'100%',
                display:"inline-block",
                marginLeft:'10px'
              }} onClick={this.cancelPipeSetter.bind(this)}>
              Cancel
            </button>
          </div>
        </Collapse>

        <img className="piped-image"
            src={that.state.pipedImageUrl}
            onLoad={(e)=>{e.currentTarget.style.display=''}}
            onError={(e)=>{e.currentTarget.style.display='none'}}
            style={{
            width: "100%",
        }}
            onClick={this.onClickImage.bind(this)}/>

     </div>
    );
  }
}


// action
function updatePanel(panelId, panel){
  return({
    type:"UPDATE_PANEL",
    panelId: panelId,
    panelInfo: panel,
  });
}

// redux
function mapStateToProps(state){
  return({
    videoName: state.vtoonData.videoName,
    selectedPanel: state.vtoonData.panels[state.vtoonData.selectedPanelId],
    selectedPanelId: state.vtoonData.selectedPanelId,
    workerId: state.vtoonData.worker,
  })
}

function mapDispatchToProps(dispatch){
  return({
    _updatePanel:(panelId,panel)=>{dispatch(updatePanel(panelId,panel))}
  })
}

// class
Pipe = connect(mapStateToProps,mapDispatchToProps)(Pipe);
export default Pipe;

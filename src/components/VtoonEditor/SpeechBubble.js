import React from 'react';
import {connect} from 'react-redux';
import $ from 'jquery';
import 'jquery-ui/themes/base/core.css';
import 'jquery-ui/themes/base/draggable.css';
import 'jquery-ui/ui/core';
import 'jquery-ui/ui/widgets/draggable';

// import {Editor, EditorState,ContentState, RichUtils} from 'draft-js';

class SpeechBubble extends React.Component {
  constructor(props){
    super(props);
    this.onChange = (editorState) => {
      this.setState({editorState});
    }
    this.handleKeyCommand = this.handleKeyCommand.bind(this);
  }

  componentDidMount(){

      $("#"+this.props.id+"-"+this.props.index).draggable(
        {
          containment: "parent",
          scroll: false,
          start: function(event, ui) {
            event.stopPropagation();
          },
          cancel: ".text-box"});
//    var func =(e)=>{e.stopPropagation(); console.log("this");}
  }

  handleKeyCommand(command) {
    const newState = RichUtils.handleKeyCommand(this.state.editorState, command);
    if (newState) {
      this.onChange(newState);
      return 'handled';
    }
    return 'not-handled';
  }

  _onBoldClick() {
    this.onChange(RichUtils.toggleInlineStyle(this.state.editorState, 'BOLD'));
  }

  render(){
    var text ="";
    this.props.speechBubble.scripts.map((script)=>{text+= script;});
    var content = ContentState.createFromText(text);
    return (
      <div
        id={this.props.id+"-"+this.props.index}
        style={{
          position:"absolute",
          top:this.props.speechBubble.positionY,
          left:this.props.speechBubble.positionX,
          width:"150px",
          height:"80px",
          borderRadius:"3%",
          border:"2px solid #444444",
          backgroundColor:"white",
          pointerEvents: "auto",
        }}>
        <div
          className="text-box"
          style={{
            pointerEvents: "auto",
          }}
          onMouseDown={(e)=>{ e.stopPropagation(); this.script.focus();}}
          >

            {/* draft.js 이용하는 부분
              <Editor
              ref={ref=> this.script =ref}
              editorState={EditorState.createWithContent(content)}
              handleKeyCommand={this.handleKeyCommand}
              onChange={this.onChange}
            />*/}

        </div>
      </div>
    )
  }
}

// redux
function mapStateToProps(state){
  return({
    pages: state.vtoonData.pages,
    panels: state.vtoonData.panels,
    speechBubbles: state.vtoonData.speechBubbles,
  });
}

function mapDispatchToProps(dispatch){
  return {

  }
}

SpeechBubble = connect(mapStateToProps,mapDispatchToProps)(SpeechBubble);
export default SpeechBubble;

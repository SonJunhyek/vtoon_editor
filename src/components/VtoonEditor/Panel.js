import React from 'react';
import { connect } from 'react-redux';

import $ from 'jquery';
import 'jquery-ui/themes/base/core.css';
import 'jquery-ui/themes/base/draggable.css';
import 'jquery-ui/ui/core';
import 'jquery-ui/ui/widgets/draggable';

class Panel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      originX: null,
      originY: null,
      isSelected: false,
      x1: 0,
      y1: 0,
      x2: 400,
      y2: 0,
      x3: 400,
      y3: 225,
      x4: 0,
      y4: 225,
      cropEditOn: false,

      //clip-path
      clipPathX1: 0,
      clipPathX2: 0,
      clipPathY1: 0,
      clipPathY2: 0,
    };
  }

  componentDidMount() {
    let that=this;
    // make image(panel) movable and resizable
    $("#" + this.props.id).draggable({
//      containment: "parent",
      scroll: false,
      cancel: "span, .text-box, .crop"+", #speech-"+this.props.id.split("-")[1],
      stop: (event,ui) => { // dom 정보와 virtual dom 정보 일치시킴

        // make updated panel
        let imageCSSTemp = Object.assign({},that.props.panels[that.props.id].imageCSS);
        imageCSSTemp.positionX = ui.position.left;
        imageCSSTemp.positionY = ui.position.top;
        let panelTemp = Object.assign({},that.props.panels[that.props.id],{imageCSS: imageCSSTemp});
        // update panel to store
        that.props._updatePanel(that.props.id,panelTemp);
      }
    }).resizable({
      aspectRatio: true,
      stop: (event,ui) => { // dom 정보와 virtual dom 정보 일치시킴
          console.log(ui);
          // make updated panel
          let imageCSSTemp = Object.assign({},that.props.panels[that.props.id].imageCSS);
          imageCSSTemp.positionX = ui.position.left;
          imageCSSTemp.positionY = ui.position.top;
          imageCSSTemp.height = ui.size.height;
          imageCSSTemp.width = ui.size.width;
          let panelTemp = Object.assign({},that.props.panels[that.props.id],{imageCSS: imageCSSTemp});
          // update panel to store
          that.props._updatePanel(that.props.id,panelTemp);
        }
    });

    // make crop frame movable and resizable
    $("#crop-"+this.props.id.split("-")[1]).
    draggable({
      containment: "#" + this.props.id,
      stop: (e,ui) => { // dom 정보와 virtual dom 정보 일치시킴
        console.log("T",e.target.style.width);
        let x1 = ui.position.left/that.props.panel.imageCSS.width;
        let x2 =
        (ui.position.left +
        parseInt(e.target.style.width.split('px')[0])) /  that.props.panel.imageCSS.width;
        let y1 = ui.position.top/that.props.panel.imageCSS.height;
        let y2 =
        (ui.position.top + parseInt(e.target.style.height.split('px')[0])) /  that.props.panel.imageCSS.height;
        console.log(ui.position.left, e.target.style.width,that.props.panel.imageCSS.width);

        // make updated panel
        let cropPointsTemp = [x1,x2,y1,y2];
        let panelTemp = Object.assign({},that.props.panels[that.props.id],{cropPoints: cropPointsTemp});

        // update panel to store
        that.props._updatePanel(that.props.id,panelTemp);
      }
    }).resizable({
      containment: "parent",
      handles: "sw",
      stop: (event,ui) => { // dom 정보와 virtual dom 정보 일치시킴
        // make updated panel
        let x1 =  ui.position.left/that.props.panel.imageCSS.width;// 좌변 위치 변경 사항
        let y2 =
        (ui.position.top + ui.size.height) /  that.props.panel.imageCSS.height; // 하변 위치 변경사항

        let cropPointsTemp = [...that.props.panel.cropPoints];
        cropPointsTemp[0] = x1;
        cropPointsTemp[3] = y2;

        let panelTemp = Object.assign({},that.props.panels[that.props.id],{cropPoints: cropPointsTemp});

        // update panel to store
        that.props._updatePanel(that.props.id,panelTemp);
      }
    });
  }

  // 초기 위치 값을 알아냄
  // 마우스 포인터의 변위 만큼 transition
  render() {
    let that = this;
    let video = document.getElementById("video");
    let panel = this.props.panel;

    let x1 = panel.cropPoints[0]*panel.imageCSS.width;
    let x2 = panel.cropPoints[1]*panel.imageCSS.width;
    let y1 = panel.cropPoints[2]*panel.imageCSS.height;
    let y2 = panel.cropPoints[3]*panel.imageCSS.height;

    return (
      <div id = {this.props.id}
      style = {
        {
          position: "absolute",
          width: this.props.panel.imageCSS.width,
          height: this.props.panel.imageCSS.height,
          top: this.props.panel.imageCSS.positionY,
          left: this.props.panel.imageCSS.positionX,
        }
      }
      draggable = "true"

      onClick = {
        (e) => {
          that.setState({
            cropEditOn: that.state.cropEditOn ? false : true,
          })

          // this.props.id 를  selectedpanel로 store에 저장
          // videoView 에서 panel window로 아이디 값을 넘기면 이에 따라 window 갱신
          that.props._onSelectPanel(this.props.id);
          // 선택한 이미지 앞의 이미지 시점부터 영상 플레이
          video.currentTime = this.props.panel.metaData.time;
          video.play();
        }
      } >


      <img
        src={this.props.panel.imageUrls[this.props.panel.imageUrls.length - 1]}
        style={{
          position: "absolute",
          opacity : "0.2",
          maxWidth: "100%",
          height: "auto",
          display: that.state.cropEditOn ? "" : "none",
        }}
      />

      <img
        src={this.props.panel.imageUrls[this.props.panel.imageUrls.length - 1]}
        style={
          {
            maxWidth: "100%",
            height: "auto",
            clipPath: "polygon(" +
              x1 + "px " + y1 + "px," +
              x2 + "px " + y1 + "px," +
              x2 + "px " + y2 + "px," +
              x1 + "px " + y2 + "px)",
          }
        }
      />

        <div  id={"crop-"+that.props.id.split("-")[1]}
        className="crop"
        style={{
          position: "absolute",
          top: panel.cropPoints[2]*panel.imageCSS.height, //y1*height
          left: panel.cropPoints[0]*panel.imageCSS.width, //x1*width
          width: (panel.cropPoints[1]-panel.cropPoints[0])*panel.imageCSS.width, // (x2-x1)*width,
          height: (panel.cropPoints[3]-panel.cropPoints[2])*panel.imageCSS.height, // (y2-y1)*height,
          display: that.state.cropEditOn ? "" : "none",
        }}
        onClick = {
          (e)=>{
            e.stopPropagation();
          }
        }
        >
          <div className="borderLine"
          style={{
            width: "100%",
            height: "100%",
            border: "4px solid paleGreen",
            webkitBoxSizing: "border-box",
          }}
          onClick={(e)=>{
            that.setState({
              cropEditOn: that.state.cropEditOn ? false : true,
            })
          }}
          />
        </div>
      </div>
    )
  }
}
//TODO. CROP수정 켜졌을때 다른 이벤트 블락하기
//crop의 위치정보, 크기정보에 따라 clipPath 지정

// action
function postPanelId(panelId) {
  return ({
    type: "POST_SELECTED_PANEL",
    selectedPanelId: panelId,
  })
}

function updatePanel(panelId, panel) {
  return ({
    type: "UPDATE_PANEL",
    panelId: panelId,
    panelInfo: panel,
  })
}

// redux
function mapStateToProps(state) {
  return ({
    pages: state.vtoonData.pages,
    panels: state.vtoonData.panels,
    speechBubbles: state.vtoonData.speechBubbles,
  });
}

function mapDispatchToProps(dispatch) {
  return {
    _onSelectPanel: (panelId) => {
      dispatch(postPanelId(panelId));
    },
    _updatePanel: (panelId,panel) => {
      dispatch(updatePanel(panelId, panel));
    },
  }
}

Panel = connect(mapStateToProps, mapDispatchToProps)(Panel);
export default Panel;

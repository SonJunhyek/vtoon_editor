import React from 'react';
import { connect } from 'react-redux';
import $ from 'jquery';
import 'jquery-ui/themes/base/core.css';
import 'jquery-ui/themes/base/draggable.css';
import 'jquery-ui/themes/base/resizable.css';
import 'jquery-ui/ui/core';
import 'jquery-ui/ui/widgets/draggable';
import 'jquery-ui/ui/widgets/resizable';

class WindowContainer extends React.Component {
  constructor(props) { super(props); }

  componentDidMount(){ $(".window-container").draggable().resizable(); }

  render(){
    var showModal = this.props.showWindow ? "":"none" ;
    return(
      <div className="window-container"
        style={{
          position:"fixed",
          top: "10%",
          left: "50%",
          width: "300px",
          height: "300px",
          backgroundColor: "white",
          borderRadius: "5px",
          boxShadow: "2px 2px 10px #555555",
          zIndex:"20",
          display:showModal,
        }}
      > {this.props.contents}
      </div>
    )
  }
}

// action
function close() {
  return ({
    type: "CLOSE_WINDOW",
  })
}

// redux
function mapStateToProps(state) {
  return ({
    showWindow: state.vtoonData.showWindow,
  });
}

function mapDispatchToProps(dispatch) {
  return {
    _close: ()=>{dispatch(close())},
  }
}

WindowContainer = connect(mapStateToProps, mapDispatchToProps)(WindowContainer);
export default WindowContainer;

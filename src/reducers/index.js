import { combineReducers } from 'redux';
import vtoonData from './vtoonData';

const rootReducer = combineReducers({
    vtoonData,
});

export default rootReducer;

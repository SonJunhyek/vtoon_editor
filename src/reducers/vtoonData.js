const initialState = {
 // store

  // worker
    workerId:"",

  // Video Data
    videoName: "",
    videoInfo: {url:""},

  // page size
    pageWidth: "",
    pageHeight: "",

  // panel size
    panelWidth: 400,
    panelHeight: null,
    panelPositionX: 100,
    panelPositionY: 37.5,

  // pages
    pages: [],
    pageIdAllocator: null,

  // panels
    panels: {},
    panelIdAllocator: null,
    selectedPanelId: null,

  // speech Bubble
    speechBubbles: {},
    speechBubbleldAllocator: null,

  // modal state
    modalContents: null,
    showModal:false,

  // window state
    windowContents: null,
    showWindow:false,

  // publish
    publish : {
      title: null,
      transition: [],
      fpp: [],
      images: [
        [], // 중첩 될 이미지의 어레이
      ],
      resolution: [
        800,
        0
      ],
      descriptions: ""
    }
};

const vtoonDataReducer = (state=initialState, action) => {

/* Samples of copying element in store
 * let pagesTemp = [...state.pages];
 * let panelsTemp = Object.assign({},state.panelsBubbles);
 * let speechBubblesTemp = Object.assign({},state.speechBubbles);
 */
  switch(action.type){

  // Intialization
    case "INIT_STORE":
      return Object.assign({},initialState)

  // Load Data
    case "POST_VIDEO":
      return Object.assign({}, state, {
        videoName: action.videoName,
        videoInfo: action.videoInfo,
      })

    case "POST_ALL":
      debugger;
      return Object.assign({},state,{
        pages: [...state.pages, ...action.pages],
        panels: Object.assign({}, state.panels, action.panels),
        speechBubbles: Object.assign({}, state.speechBubbles, action.speechBubbles),
        pageIdAllocator: action.lastIndex,
        panelIdAllocator: action.lastIndex,
        speechBubbleldAllocator: action.lastIndex,
      })

  // Worker
    case "POST_WORKER_ID":
      return Object.assign({},state,{ workerId: action.workerId })

  // Page
    case "POST_PAGE":
      return Object.assign({},state,{
        pages:[...state.pages, ...action.pages]
      })

    case "ADD_PAGE":
      let pagesTemp = [...state.pages];
      action.pageInfo.startTime = pagesTemp[action.index].endTime;
      action.pageInfo.endTime = pagesTemp[action.Index+1] ? pagesTemp[action.Index+1].startTime : action.pageInfo.endTime;
      action.pageInfo.startFrame = pagesTemp[action.index].endFrame;
      action.pageInfo.endFrame = pagesTemp[action.Index+1] ? pagesTemp[action.Index+1].startFrame : action.pageInfo.endFrame;
      console.log(action.pageInfo);
      pagesTemp.splice(action.index+1, 0, action.pageInfo )
      return Object.assign({}, state,{
        pages: pagesTemp,
        pageIdAllocator: state.pageIdAllocator+1,
      })

    case "REMOVE_PAGE":{
      let pagesTemp = [...state.pages];
      pagesTemp.splice(action.pageIndex,1);
      return Object.assign({}, state,{
        pages: pagesTemp,
        pageIdAllocator: state.pageIdAllocator-1,
      })
    }

    case "MERGE_PAGES":{
      let pagesTemp = [...state.pages];
      action.newPage.height += action.mergedPage.height;
      action.newPage.endTime = action.mergedPage.endTime;
      action.newPage.endFrame = action.mergedPage.endFrame;
      action.newPage.panels = [...action.newPage.panels, ...action.mergedPage.panels];
      action.newPage.speechBubbles = [...action.newPage.speechBubbles, ...action.mergedPage.speechBubbles];
      console.log(action.newPage);
      pagesTemp.splice(action.index,2,action.newPage);
      console.log(pagesTemp);
      console.log(state.panels);
      console.log(state.speechBubbles);

      return Object.assign({},state,{
        pages: pagesTemp
      })
    }

    case "RESIZE_PAGE":{
      let pagesTemp = [...state.pages];
      let newPage = Object.assign({},pagesTemp[action.pageIndex], { height: action.height});
      console.log(action.pageIndex);
      pagesTemp.splice(action.pageIndex,1,newPage);
      return Object.assign({}, state, {
        pages: pagesTemp
      })
    }

  // Panel
    case "POST_PANEL":{
      let panelTemp={};
      let pagesTemp = [...state.pages];
      let newPage =
        Object.assign({},pagesTemp[action.pageIndex],{
          panels: [...pagesTemp[action.pageIndex].panels, action.panelId ]
        }); // modify a page that adds new panel
      pagesTemp.splice(action.pageIndex,1,newPage);
      panelTemp[action.panelId]=action.panelInfo;

      return Object.assign({},state,{
        pages: pagesTemp,
        panels: Object.assign({},state.panels, panelTemp),
        panelIdAllocator: state.panelIdAllocator+1,
      })
    }

    case "UPDATE_PANEL":{
      let panelTemp = {};
      panelTemp[action.panelId]=action.panelInfo;
      return Object.assign({},state,{panels:Object.assign({},state.panels,panelTemp)})
    }

  // Speech Bubble
    case "POST_SPEECH_BUBBLE":{
      let speechBubble={};
      speechBubble[action.speechBubbleId]=action.speechBubble;
      return Object.assign({},state,{
        speechBubbles: Object.assign({},state.speechBubbles,speechBubble)
      })
    }

  // Publish
    case "POST_PUBLISH_DATA":
      return Object.assign({},state,{publish: action.publishData})

  // Selected Panel
    case "POST_SELECTED_PANEL":
      return Object.assign({}, state, {selectedPanelId: action.selectedPanelId} )

    case "UPDATE_SELECTED_PANEL" :{
      let panelTemp = {};
      panelTemp[state.selectedPanelId]=action.panel;
      return Object.assign({}, state, {
        panels: Object.assign({}, state.panels, panelTemp),
      })
    }

  // Modal
    case "OPEN_MODAL":
      return Object.assign({}, state, {
        modalContents: action.modalContents,
        showModal:true,
      })

    case "CLOSE_MODAL":
      return Object.assign({}, state, {
        showModal:false
      })

  // Window
    case "OPEN_WINDOW":
      return Object.assign({}, state, {
        windowContents: action.windowContents,
        showWindow:true,
      })

    case "CLOSE_WINDOW":
      return Object.assign({}, state, {
        showWindow:false
      })

  // Default
    default:
      return state;
  }
};

export default vtoonDataReducer;
